# CTB SR
[![Discord](https://discordapp.com/api/guilds/667804727639015462/widget.png)](https://discord.gg/PQknGc5)
[![SemVer](https://img.shields.io/badge/SemVer-Versioning-black)](http://semver.org)


CTB SR is an alternative Ranking/Skill comparison system for osu!catch. The system is based off Overwatch's SR system & the osu!catch PP system.


### License
This project uses the MIT Licence
Permission is hereby granted, free of charge, to any person obtaining a copy.