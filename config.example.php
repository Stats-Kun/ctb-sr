<?php
$info = [
    'name' => 'osu!catch SR',
    'description' => 'osu!catch SR Calculator',
    'url' => '',
    'timezone' => 'Europe/London'
];

$database = [
    'host' => 'localhost',
    'port' => '3306',
    'username' => '',
    'password' => '',
    'database' => 'osuSR'
];

$osu = [
    'authEndpoint' => 'https://osu.ppy.sh/oauth/authorize',
    'tokenEndpoint' => 'https://osu.ppy.sh/oauth/token',
    'apiEndpoint' => 'https://osu.ppy.sh/api/v2',
    'returnURI' => '',
    'clientID' => '',
    'clientSecret' => ''
];
?>