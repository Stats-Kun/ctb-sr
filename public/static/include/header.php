<?php
session_start();

include_once dirname($_SERVER['DOCUMENT_ROOT']) . '/config.php';
include_once dirname($_SERVER['DOCUMENT_ROOT']) . '/functions.php';
?>

<!DOCTYPE html>
<html>
    <head>
        <!-- Heading Data -->
        <title><?=$info['name']?></title>
        <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta property="og:title" content="<?=$info['name']?>" />
	    <meta property="og:description" content="<?=$info['description']?>" />
	    <meta property="og:image" content="/static/media/logo.png">
	    <meta property="og:url" content="<?=$info['url']?>" />
        <link rel="shortcut icon" href="/static/media/favicon.ico" type="image/x-icon">
        <link rel="icon" href="/static/media/favicon.ico" type="image/x-icon">
        <!-- Stylesheets -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
	    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:400,700,900&display=swap">
	    <link rel="stylesheet" href="/static/style/animate.css">
	    <link rel="stylesheet" href="/static/style/main.css">
        <!-- Scripts -->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    </head>
    <body>