/* * * * * * * * * * * * * * *
 *  osu!catch SR Calculation *
 * * * * * * * * * * * * * * */

function getSR(sr, ar, maxCombo, playerCombo, acc, missCount) {
    var starSR = starConversion(sr);
    var lengthSR = lengthBonus(maxCombo);
    var missSR = missPenalty(missCount, maxCombo);
    var comboSR = comboPenalty(playerCombo, maxCombo);
    var arSR = arBonus(ar);
    var accSR = accuracyPenalty(acc);

    var finalCalc = starSR * lengthSR * missSR * comboSR * arSR * accSR;

    return finalSR(finalCalc);
}


function starConversion(sr) {
    if (sr < 2.00) return (600 * sr);
    else if (sr < 2.70) return ((500/0.7) * sr) - (1600/7);
    else if (sr < 4.00) return ((400/1.3) * sr) + (11300/13);
    else if (sr < 5.30) return ((500/1.3) * sr) + (7300/13);
    else if (sr < 6.50) return ((600/1.2) * sr) - 50;
    else return (3200 + 400*(sr-6.5));
}

function lengthBonus(mc) {
		if (mc<=1000) return 0.60 + (0.1 * (mc/250))
		if (mc<=5500) return 1 + (0.05 * (mc-1000)/2000)
        if (mc>5500) return 1.125
}

function missPenalty(mCount, mCombo) {
    return 1 - Math.pow((mCount / mCombo), 0.8);
}

function comboPenalty(pCombo, mCombo) {
    return Math.pow((pCombo / mCombo), 0.2);
}

function accuracyPenalty(acc) {
    return Math.pow(acc / 100, 4);
}

function arBonus(ar) {
    if (ar < 8) return 1 + 0.025 * (8.0 - ar);
    else if (ar > 9) return 1 + 0.05 * (ar - 9.0);
}